-- -----------------------------------------------------
-- Schema e-commerce-motorcycle-equipment
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `e-commerce-motorcycle-equipment`;

CREATE SCHEMA `e-commerce-motorcycle-equipment`;
USE `e-commerce-motorcycle-equipment` ;

-- -----------------------------------------------------
-- Table `e-commerce-motorcycle-equipment`.`product_category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e-commerce-motorcycle-equipment`.`product_category` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `category_name` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE=InnoDB
AUTO_INCREMENT = 1;

-- -----------------------------------------------------
-- Table `e-commerce-motorcycle-equipment`.`product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `e-commerce-motorcycle-equipment`.`product` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `sku` VARCHAR(255) DEFAULT NULL,
  `name` VARCHAR(255) DEFAULT NULL,
  `description` VARCHAR(255) DEFAULT NULL,
  `unit_price` DECIMAL(13,2) DEFAULT NULL,
  `image_url` VARCHAR(255) DEFAULT NULL,
  `size` VARCHAR(255) DEFAULT NULL,
  `active` BIT DEFAULT 1,
  `units_in_stock` INT(11) DEFAULT NULL,
  `date_created` DATETIME(6) DEFAULT NULL,
  `last_updated` DATETIME(6) DEFAULT NULL,
  `category_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_category` (`category_id`),
  CONSTRAINT `fk_category` FOREIGN KEY (`category_id`) REFERENCES `product_category` (`id`)
) 
ENGINE=InnoDB
AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Add sample data
-- -----------------------------------------------------

INSERT INTO PRODUCT_CATEGORY(CATEGORY_NAME) VALUES ('HELMETS');
INSERT INTO PRODUCT_CATEGORY(CATEGORY_NAME) VALUES ('JACKETS');
INSERT INTO PRODUCT_CATEGORY(CATEGORY_NAME) VALUES ('GLOVES');

INSERT INTO PRODUCT (SKU, NAME, DESCRIPTION, IMAGE_URL, SIZE, ACTIVE, UNITS_IN_STOCK,
UNIT_PRICE, CATEGORY_ID,DATE_CREATED)
VALUES ('Ls2-Verso-Plane-99', 'Ls2 Verso Plane Of 570 Matte Black', 'She has glasses and a hat',
'assets/images/products/helmets/helmet-ddmoto-1001.jpg'
,'XL',1,5,99.99,1, NOW());

INSERT INTO PRODUCT (SKU, NAME, DESCRIPTION, IMAGE_URL, SIZE, ACTIVE, UNITS_IN_STOCK,
UNIT_PRICE, CATEGORY_ID,DATE_CREATED)
VALUES ('LS2 Valiant 2 109', 'LS2 Valiant 2 Flip-up FF900', 'She has glasses and a pinlock',
'assets/images/products/helmets/helmet-ddmoto-1002.jpg'
,'L',1,3,289.99,1, NOW());

INSERT INTO PRODUCT (SKU, NAME, DESCRIPTION, IMAGE_URL, SIZE, ACTIVE, UNITS_IN_STOCK,
UNIT_PRICE, CATEGORY_ID,DATE_CREATED)
VALUES ('LS2 Strobe 109', 'LS2 Strobe Flip-up FF325', 'She has glasses.',
'assets/images/products/helmets/helmet-ddmoto-1003.jpg'
,'M',1,10,109.99,1, NOW());

INSERT INTO PRODUCT (SKU, NAME, DESCRIPTION, IMAGE_URL, SIZE, ACTIVE, UNITS_IN_STOCK,
UNIT_PRICE, CATEGORY_ID,DATE_CREATED)
VALUES ('SHIMA Hero 199', 'SHIMA Hero Three-layer JACKET three-layer', 'Waterproof, ARMOR PLUS Protectors,',
'assets/images/products/jackets/jacket-ddmoto-1001.jpg'
,'2XL',1,4,199.99,2, NOW());

INSERT INTO PRODUCT (SKU, NAME, DESCRIPTION, IMAGE_URL, SIZE, ACTIVE, UNITS_IN_STOCK,
UNIT_PRICE, CATEGORY_ID,DATE_CREATED)
VALUES ('Shima Combat 179', 'Shima Combat three layer jacket', 'Waterproof, armor plus protein',
'assets/images/products/jackets/jacket-ddmoto-1002.jpg'
,'L',1,3,179.99,2, NOW());

INSERT INTO PRODUCT (SKU, NAME, DESCRIPTION, IMAGE_URL, SIZE, ACTIVE, UNITS_IN_STOCK,
UNIT_PRICE, CATEGORY_ID,DATE_CREATED)
VALUES ('SHIMA HORIZON 269', 'SHIMA HORIZON Grey three layer JACKET', 'Waterproof, ARMOR PLUS Protectors',
'assets/images/products/placeholder.png'
,'XL',1,4,269.99,2, NOW());

INSERT INTO PRODUCT (SKU, NAME, DESCRIPTION, IMAGE_URL, SIZE, ACTIVE, UNITS_IN_STOCK,
UNIT_PRICE, CATEGORY_ID,DATE_CREATED)
VALUES ('Shima x-Break 65', 'Shima x-Break 2 Summer Moto Gloves', 'With reinforcements',
'assets/images/products/gloves/glove-ddmoto-1001.jpg'
,'L',1,6,65.00,3, NOW());

INSERT INTO PRODUCT (SKU, NAME, DESCRIPTION, IMAGE_URL, SIZE, ACTIVE, UNITS_IN_STOCK,
UNIT_PRICE, CATEGORY_ID,DATE_CREATED)
VALUES ('SHIMA Spark 65', 'SHIMA Spark Summer Leather Gloves', 'reinforcements, perforated leather',
'assets/images/products/gloves/glove-ddmoto-1002.jpg'
,'M',1,7,65.00,3, NOW());

INSERT INTO PRODUCT (SKU, NAME, DESCRIPTION, IMAGE_URL, SIZE, ACTIVE, UNITS_IN_STOCK,
UNIT_PRICE, CATEGORY_ID,DATE_CREATED)
VALUES ('SHIMA RS-2 65', 'SHIMA RS-2 Red Leather Sports Gloves', ', Armor Plus reinforcement',
'assets/images/products/gloves/glove-ddmoto-1003.jpg'
,'2XL',1,2,65.00,3, NOW());