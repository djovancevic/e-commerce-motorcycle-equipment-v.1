package com.ad.it.ecommercemotorcycleequipment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ECommerceMotorcycleEquipmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(ECommerceMotorcycleEquipmentApplication.class, args);
	}

}
